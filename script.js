const navScroll = () => {
    let nav = document.querySelector('nav');
    window.onscroll = () => {
        const top = window.scrollY;
        if (top > 100) {
            nav.classList.add('scrolled');
        }
        else {
            nav.classList.remove('scrolled');
        }
    }
}

const menuActive = () => {
    let menu = document.querySelector('.menu');
    let hamburger = document.querySelector('.hamburger');
    let menuLinks = document.querySelectorAll('.menu li');

    hamburger.addEventListener('click', () => {
        menuRemove(menu, hamburger);
    });

    Array.from(menuLinks).map(menuLink => {
        menuLink.addEventListener('click', () => {
            menuRemove(menu, hamburger);
        })
    });
}
const menuRemove = (menu, hamburger) => {
    menu.classList.toggle('menu-active');
    hamburger.classList.toggle('animation');
}

const questionsToggle = () => {
    let questions = document.querySelectorAll('.questions');

    questions.forEach(question => {
        let btn = question.querySelector('.flex-question');
        let btnOpen = question.querySelector('.icon-arrow');
        let btnClose = question.querySelector('.icon-close');
        let paragraph = question.querySelector('p');

        btn.addEventListener('click', () => {
            paragraph.classList.toggle('display');
            paragraph.classList.toggle('opacity');
            btnClose.classList.toggle('display');
            btnOpen.classList.toggle('hide');
        })
    })
}

const infoFeatures = () => {
    let features = document.querySelectorAll('.onglets');
    let infoFeatures = document.querySelectorAll('.info-features');

    features.forEach((feature, indexFeature) => {
        feature.addEventListener('click', () => {
            //click same onglet => stop the function with the return
            if(feature.classList.contains('active')){
                return;
            } else {
                feature.classList.add('active');
            }

            let index = feature.getAttribute('data-animation');
            Array.from(features).map(refeature => {
                if (refeature.getAttribute('data-animation') != index) {
                    refeature.classList.remove('active');
                }
            })

            infoFeatures.forEach((infoFeature, indexInfo) => {
                if(indexFeature == indexInfo){
                    infoFeature.classList.add('display');
                    infoFeature.classList.remove('hide');
                    infoFeature.classList.add('activeContenu');
                }
                else {
                    infoFeature.classList.remove('display');
                    infoFeature.classList.add('hide');
                    infoFeature.classList.remove('activeContenu');
                }
            })
        })
    })
}

const email = () => {
    let form = document.getElementById('form');
    let small = document.querySelector('.smallError');
    let btnRed = document.querySelector('form .btn-red');

    form.addEventListener('submit', (e) => {
        e.preventDefault();
        let email = form['email'].value;

        if (!email) {
            form.classList.add('error');
            small.innerHTML = "Whoops, it can't be empty";
            small.style.marginBottom = "20px";
            mediaError(btnRed);
        }
        else if (!validateEmail(email)) {
            form.classList.add('error');
            small.innerHTML = "Whoops, make sure it's an email";
            small.style.marginBottom = "20px";
            mediaError(btnRed);
        }
        else {
            form.classList.remove('error');
            btnRed.style.marginTop = "0";
        }
    })
}
const mediaError = btnRed => {
    if (window.innerWidth < 790) {
        btnRed.style.marginTop = "40px";
    }
}
const validateEmail = email => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


const gsapEvent = () => {
    const header = document.querySelector('header');
    const left = document.querySelector('.left');
    const right = document.querySelector('.right');

    const extension = document.getElementById('extension');
    const one = document.querySelector('.one');
    const two = document.querySelector('.two');
    const three = document.querySelector('.three');

    let TL = gsap.timeline({
        scrollTrigger: { trigger: header }
    });
    TL
        .from(left, 1, {x: -300, opacity: 0})
        .from(right, 1, {x: 300, opacity: 0 }, "=-0.9")

    let TLextension = gsap.timeline({
        scrollTrigger: { trigger: extension }
    });
    TLextension
        .from(one, 0.7, {y: -200, opacity: 0})
        .from(two, 0.7, {y: -200, opacity: 0 }, "=-0.3")
        .from(three, 0.7, {y: -200, opacity: 0 }, "=-0.3")
}


const app = () => {
    gsapEvent();
    navScroll();
    menuActive();
    questionsToggle();
    infoFeatures();
    email();
}
app();
